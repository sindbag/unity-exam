using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_PlayerMovement : MonoBehaviour
{

    public float MovementSpeed = 1.0f;
    public GameObject Player;
    public Rigidbody2D rb;

    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        if (h != 0) Move(h);
    }

    void Move(float Direction)
    {
        Vector3 NewPos = new Vector2 (rb.position.x + MovementSpeed * Time.deltaTime * Direction, rb.position.y);
        float SizeX = 160.0f / 9.0f - 5.0f;
        NewPos.x = Mathf.Clamp(NewPos.x,-SizeX, SizeX);
        rb.MovePosition(NewPos);
    }
}
