using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_Scoring : MonoBehaviour
{
    public float Score = 1.0f;
    S_ScoreManager ScoreManager;

    void Start()
    {
        ScoreManager = Object.FindObjectOfType<S_ScoreManager>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        S_BallMovement Ball = collision.gameObject.GetComponent<S_BallMovement>();
        if (Ball != null)
        {
            ScoreManager.ChangeScore(Score);
        }
    }
}
