using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_BallMovement : MonoBehaviour
{
    public float MovementSpeed = 100.0f;
    Vector2 MovementDirection;

    public GameObject Ball;
    public Rigidbody2D rb;


    Vector3 PreviosPos;

    void Start()
    {
        MovementDirection = new Vector2(Random.value, Random.value);
        MovementDirection.Normalize();
        rb.velocity = MovementDirection * MovementSpeed;
    }

    private void FixedUpdate()
    {
        rb.velocity = rb.velocity.normalized * MovementSpeed;
    }
}
