using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class S_ScoreManager : MonoBehaviour
{
    public float CurrentScore = 0;
    public  Text UI;

    public void ChangeScore (float Delta)
    {
        CurrentScore += Delta;
        UI.text = CurrentScore.ToString();
    }
}
